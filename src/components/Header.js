import React from "react";
import anh4 from "../assets/images/anh4.jpg";

export const Header = () => {
  return (
    <div className="app">
      <div className="app-header">
        <div className="app-header-background">
          <header className="app-nav-list-item">
            <nav className="app-header-nav">
              {/* hàng ul thứ nhất */}
              <ul className="app-header-nav-list">
                <li className="app-header-nav-item-summer">
                  <a href="#" className="app-header-nav-summer">
                    SUMMER
                    <br />
                    <span className="app-header-nav-and">and</span>
                    STORM
                  </a>
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  new arrivals
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  limited edition
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  baby
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  kids
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  shop the look
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  women
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  s&s home
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  sale
                </li>
                <li className="app-header-nav-item app-header-nav-text">
                  gift card
                </li>
              </ul>

              {/* hàng ul thứ 2 */}
              <ul className="app-header-nav-list">
                <li className="app-header-nav-item-icon app-header-nav-item-icon-list">
                  <a href="#" className="app-header-nav-link">
                    <i className="app-header-nav-icon-search fas fa-search"></i>
                  </a>
                </li>
                <li className="app-header-nav-item-icon">
                  <a href="#" className="app-header-nav-link">
                    <i className="app-header-nav-icon-heart fas fa-heart"></i>
                  </a>
                </li>
                <li className="app-header-nav-item-icon">
                  <a href="#" className="app-header-nav-link">
                    <i className="app-header-nav-icon-lock fas fa-lock"></i>
                  </a>
                </li>
                <li className="app-header-nav-item-icon">
                  <a href="#" className="app-header-nav-link">
                    <i className="app-header-nav-icon-user fas fa-user-circle"></i>
                  </a>
                </li>
                <li className="app-header-menu">
                  <a>
                    <i className="fas fa-bars"></i>
                  </a>
                </li>
              </ul>
            </nav>
            <div className="app-header-photo">
              <div>
                <img src={anh4}></img>
                <h4 className="app-header-photo-title">
                  NEW SUMMER
                  <br />
                  <span>ARRIVALS SALE NOW</span>
                </h4>
                <button className="app-header-photo-title-btn">SHOP NOW</button>
                <h4 className="discover-text">DISCOVER MORE</h4>
              </div>
            </div>
          </header>
        </div>
      </div>
    </div>
  );
};

export default Header;

{
  /* <div>
<header className="App-header">
  <nav className="App-header--nav">
    <ul className="App-header--nav-list">
      <li className="app-header-nav-item">
        <a href="#" className="App-header--nav-link">
          SUMMER and STORM
        </a>
      </li>
      <li className="App-header--nav-list-item">new arrivals</li>
      <li className="App-header--nav-list-item">limited edition</li>
      <li className="App-header--nav-list-item">baby</li>
      <li className="App-header--nav-list-item">kids</li>
      <li className="App-header--nav-list-item">shop the look</li>
      <li className="App-header--nav-list-item">women</li>
      <li className="App-header--nav-list-item">s&s home</li>
      <li className="App-header--nav-list-item">sale</li>
      <li className="App-header--nav-list-item">gift card</li>
    </ul>
    <ul className="App-header--nav">
      <li className="App-header--nav-list-item">
        <i className="fas fa-search"></i>
      </li>
      <li className="App-header--nav-list-item">
        <i className="fas fa-heart"></i>
      </li>
      <li className="App-header--nav-list-item">
        <i className="fas fa-lock"></i>
      </li>
      <li className="App-header--nav-list-item">
        <i className="fas fa-user-circle"></i>
      </li>
    </ul>
  </nav>
</header>;
</div> */
}
