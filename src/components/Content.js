import React from "react";
import anh1 from "../assets/images/anh1.png";
import anh2 from "../assets/images/anh2.png";
import anh3 from "../assets/images/anh3.png";

export const Content = () => {
  return (
    <div className="warp">
      <div className="warp-container">
        <div className="warp-content">
          <img className="warp-content-img" src={anh2} />
          <a className="warp-content-shop-link" href="">
            SHOP KIDS
          </a>
        </div>

        <div className="warp-content">
          <img className="warp-content-img" src={anh1} />
          <a className="warp-content-shop-link" href="">
            SHOP BABY
          </a>
        </div>

        <div className="warp-content">
          <img
            className="warp-content-img warp-content-img-edition"
            src={anh3}
          />

          <a
            className="warp-content-shop-link warp-content-shop-link-edition"
            href=""
          >
            <br />
            SHOP LIMITED EDITION
          </a>
          <p className="limitted-edition">SHOP LIMITED EDITION</p>
        </div>
      </div>
    </div>
  );
};

export default Content;
