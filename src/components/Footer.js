import React from "react";

export const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-container">
        <div className="footer-top"></div>
        <div className="footer-text-btn">
          <h4 className="footer-text-content">EXPLORE</h4>
          <div className="footer-btn">
            <button className="footer-btn-arrivals">new arrivals</button>
            <button className="footer-btn-baby">baby</button>
            <button className="footer-btn-kids">kids</button>
            <button className="footer-btn-look">shop the look</button>
            <button className="footer-btn-women">women</button>
            <button className="footer-btn-sale">sale</button>
            <button className="footer-btn-home">s&s home</button>
          </div>
        </div>
        <div className="footer-detail">
          <div className="footer-top-line"></div>
          <ul className="footer-detail-list">
            <li className="footer-detail-list-item">
              <a
                className="footer-detail-link footer-detail-link-about"
                href=""
              >
                about
              </a>
            </li>
            <li className="footer-detail-list-item">
              <a
                className="footer-detail-link footer-detail-link-customer "
                href=""
              >
                customer care
              </a>
            </li>
            <li className="footer-detail-list-item">
              <a
                className="footer-detail-link footer-detail-link-returns"
                href=""
              >
                returns
              </a>
            </li>
            <li className="footer-detail-list-item">
              <a
                className="footer-detail-link footer-detail-link-stockists"
                href=""
              >
                stockists
              </a>
            </li>
            <li className="footer-detail-list-item">
              <a
                className="footer-detail-link footer-detail-link-instagram"
                href=""
              >
                instagram
              </a>
            </li>
            <li className="footer-detail-list-item">
              <a
                className="footer-detail-link footer-detail-link-newsletter"
                href=""
              >
                newsletter
              </a>
            </li>
          </ul>
          <div className="footer-bottom-line"></div>
        </div>
        <div className="footer-brand">
          <span>
            @ SUMMER <a href="">and</a> STORM
          </span>
        </div>
      </div>
    </div>
  );
};

export default Footer;
